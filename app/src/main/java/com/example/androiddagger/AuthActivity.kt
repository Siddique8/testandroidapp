package com.example.androiddagger

import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isInvisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.androiddagger.factory.ViewModelProviderFactory
import com.example.androiddagger.resource.AuthResource
import com.example.androiddagger.viewmodels.AuthViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.math.BigInteger
import javax.inject.Inject


class AuthActivity : DaggerAppCompatActivity() {
    private var TAG ="MainActivity"

    private lateinit var btnOk:Button
    private lateinit var etPassword: EditText
    private lateinit var viewModel:AuthViewModel
    private lateinit var progressBar:ProgressBar

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

//     @Inject
//     lateinit var retrofit: Retrofit

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            btnOk = findViewById(R.id.btnOk)
            etPassword = findViewById(R.id.etPassword)
            progressBar = findViewById(R.id.progressBar)
            progressBar.visibility = View.INVISIBLE

            viewModel = ViewModelProviders.of(this,providerFactory).get(AuthViewModel::class.java)

            btnOk.setOnClickListener {
               progressBar.visibility = View.VISIBLE
               var id = Integer.parseInt(etPassword.text.toString())
               authenticateUser(id)
            }



//           var userApi = retrofit.create(UserApi::class.java)
//
//           var call: Call<List<Data>> = userApi.getUsers();
//
//           call.enqueue(object : Callback<List<Data>> {
//
//               override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
//
//                   if(response.isSuccessful){
//
//                      var dataList:List<Data>? = response.body()
//
//                           Log.d(TAG,"this is the result "+dataList?.size)
//                   }
//
//               }
//
//               override fun onFailure(call: Call<List<Data>>, t: Throwable) {
//                   call.cancel()
//               }
//           })


//        var imageView = findViewById<ImageView>(R.id.imageView)
//
//        imageView.setImageDrawable(drawable)

            //This is the test of the siddique branch

    }


    private fun authenticateUser(id:Int){
        viewModel.authenticateUser(id).observe(this, Observer {
            when(it.status){

                AuthResource.AuthStatus.LOADING -> println("Loading....")
                AuthResource.AuthStatus.AUTHENTICATED -> {
                     progressBar.visibility = View.INVISIBLE
                     println("user is authenticated ${it.data.title}")
                     showToast(" User is authenticated ")
                     var intent = Intent(this,DashBoardActivity::class.java)
                     startActivity(intent)
                     finish()
                }
                AuthResource.AuthStatus.ERROR -> {
                    println("User is not Authenticated ")
                    progressBar.visibility = View.INVISIBLE
                    showToast("User is not authenticated ")
                }
                AuthResource.AuthStatus.NOT_AUTHENTICATED -> println("user is not authenticated")
            }
        })
    }


    private fun showToast(text: String){
        Toast.makeText(this,text,Toast.LENGTH_LONG).show()
    }
}
