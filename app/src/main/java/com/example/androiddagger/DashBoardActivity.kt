package com.example.androiddagger

import android.content.Intent
import android.icu.text.Edits
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddagger.database.AppExecutors
import com.example.androiddagger.database.User
import com.example.androiddagger.database.UserDataBase
import com.example.androiddagger.database.UserViewModel
import com.example.androiddagger.factory.ViewModelProviderFactory
import com.example.androiddagger.models.Data
import com.example.androiddagger.recyclerview.UserAdapter
import com.example.androiddagger.resource.Resource
import com.example.androiddagger.resource.SimpleRecyclerView
import com.example.androiddagger.viewmodels.DashViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class DashBoardActivity: DaggerAppCompatActivity() {

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

//    @Inject
//    lateinit var dataBase: UserDataBase

    private lateinit var viewModel: DashViewModel
    lateinit var recyclerView:RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dsh_board_activity)

        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        viewModel = ViewModelProviders.of(this,providerFactory).get(DashViewModel::class.java)
        var adapter = UserAdapter(this)

        //var adapter = SimpleRecyclerView()

        observePagedData(adapter)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater = menuInflater
        inflater.inflate(R.menu.dashboard_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.logOut->{
              var intent = Intent(this, AuthActivity::class.java)
              startActivity(intent)
              finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observePagedData(adapter: UserAdapter){

//        viewModel.getUsersPagedData().observe(this, Observer {
//            if (it!=null){
//                adapter?.subList(it)
//        })
        viewModel.getUsersPagedData().observe(this, Observer {
            if (it!=null){
                adapter.submitList(it)
                println("This is the Size of the it "+it.size)
                recyclerView.adapter=adapter
            }
        })


    }

    private fun observeData(){
        viewModel.observeUserData()?.observe(this, Observer {
            when(it.status){
                Resource.Status.ERROR->{  println("There is some Error ")}
                Resource.Status.LOADING->{ println("User Data is Loading ")}
                Resource.Status.SUCCESS->{

//                 for (u in it.data){
//
//                     var user = User(u.id!!,u.userId!!,u.title!!,u.isCompleted!!)
//
//                     AppExecutors.diskIO.execute(Runnable {
//                         println("This is the Thread ${Thread.currentThread()}")
//                         dataBase.userDao().insertUser(user)
//                     })
//                 }

                }
            }
        })

    }


}