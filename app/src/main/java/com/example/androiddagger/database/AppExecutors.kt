package com.example.androiddagger.database

import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object AppExecutors {


     var diskIO : Executor = Executors.newSingleThreadExecutor()
     var newtWorkIO: Executor = Executors.newFixedThreadPool(3)


//     abstract class ThreadExecutors() : Executor {
//          companion object {
//               val LOCK = Any()
//               private var instance: AppExecutors? = null
//          }
//     }


}