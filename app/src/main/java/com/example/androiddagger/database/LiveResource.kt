package com.example.androiddagger.database

import android.print.PrintJobInfo.*
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Job

@Target(AnnotationTarget.TYPE,
       AnnotationTarget.VALUE_PARAMETER,
       AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
@IntDef(STATE_STARTED,STATE_COMPLETED,STATE_FAILED)
annotation class ResourceState
data class LiveResource<T>(
    val data: LiveData<T?>,
    val state: LiveData<@ResourceState Int?>,
    val error: LiveData<Exception?>,
    val job: Job? = null
)