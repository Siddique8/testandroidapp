package com.example.androiddagger.database

import android.print.PrintJobInfo.*
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.annotation.Resource

abstract class NetworkBoundResource<ResultType,RequestType> (private val appExecutors: AppExecutors) {

    private val result = MutableLiveData<ResultType?>()
    private val error = MutableLiveData<Exception?>()

    private val state = MutableLiveData<@ResourceState Int?>()

    private val job = Job()


    init {

        val context = Dispatchers.IO
        context+job

        CoroutineScope(context).launch {

            state.postValue(STATE_STARTED)

            val dbSource = loadFromDb()

            if (shouldFetch(dbSource)){
                fetchFromNetwork(dbSource)
            }else{
                state.postValue(STATE_COMPLETED)
                result.postValue(dbSource)
            }


        }


    }

    private suspend fun fetchFromNetwork(dbSource : ResultType?){

        assert(state.value == STATE_STARTED)
        dbSource?.let(result::postValue)

        try {
            val apiResponse = makeApiCall()

            apiResponse?.let {
                saveCallResult(it)
                result.postValue(loadFromDb())
            }?: run {
                if (dbSource == null)
                     result.postValue(null)
            }

            state.postValue(STATE_COMPLETED)

        }
        catch (e: Exception){
            assert(result.value == dbSource)
            state.postValue(STATE_FAILED)
            error.postValue(e)
        }

    }



    protected abstract suspend fun loadFromDb():ResultType?

    protected abstract fun shouldFetch(data: ResultType?):Boolean

    protected abstract suspend fun makeApiCall():RequestType?

    protected abstract suspend fun saveCallResult(data: RequestType)

    fun asLiveResource() = LiveResource(result,state,error,job)




}