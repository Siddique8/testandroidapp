package com.example.androiddagger.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserInfo")
data class User(

    @PrimaryKey(autoGenerate = false)
    val id:Int,

    @ColumnInfo(name = "col_user_id")
    val userId: Int,

    @ColumnInfo(name = "col_title")
    val title: String,

    @ColumnInfo(name = "col_complete")
    val isCompleted: Boolean

) {
}