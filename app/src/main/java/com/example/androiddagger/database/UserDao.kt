package com.example.androiddagger.database

import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Query("SELECT * FROM UserInfo")
    fun getAllUsers() : LiveData<List<User>>

    @Query("SELECT * FROM UserInfo")
    fun getPagedUsers(): DataSource.Factory<Int,User>

    @Delete
    fun deleteUsers(user: User)

    @Delete
    fun deleteAllUsers(userList: List<User>)


}