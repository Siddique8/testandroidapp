package com.example.androiddagger.database

import androidx.room.Database
import androidx.room.DatabaseConfiguration
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteOpenHelper

@Database(entities = [User::class],version = 1,exportSchema = false)
abstract class UserDataBase : RoomDatabase() {

    companion object{
        val DATA_BASE_NAME= "UserDatabase"
    }

    abstract fun userDao() : UserDao

}