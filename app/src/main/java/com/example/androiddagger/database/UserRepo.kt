package com.example.androiddagger.database

import androidx.lifecycle.LiveData
import io.reactivex.annotations.SchedulerSupport.IO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserRepo {

    var dataBase: UserDataBase

    constructor(dataBase: UserDataBase) {
        this.dataBase = dataBase
    }

    fun observerUsersData() : LiveData<List<User>>?{
//        AppExecutors.diskIO.execute(Runnable {
//            println("Which thread is this ${Thread.dumpStack()}")
//            data = dataBase.userDao().getAllUsers()
//        })
        return dataBase.userDao().getAllUsers()
    }
}