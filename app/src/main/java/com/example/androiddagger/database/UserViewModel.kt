package com.example.androiddagger.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList

class UserViewModel: ViewModel {

     lateinit var dataBase: UserDataBase
     var repository: UserRepo


    constructor(dataBase: UserDataBase){
       // repository = UserRepo()
       // this.dataBase = dataBase
        repository = UserRepo(dataBase)
    }



    fun getAllUsers(): LiveData<List<User>>? {
       return repository.observerUsersData()
    }






}