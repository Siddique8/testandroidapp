package com.example.androiddagger.di

import com.example.androiddagger.AuthActivity
import com.example.androiddagger.DashBoardActivity
import com.example.androiddagger.factory.AuthViewModelsModules
import com.example.androiddagger.factory.DashBoardViewModelModules
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

     @ContributesAndroidInjector(modules = [AuthViewModelsModules::class,AuthModule::class])
     abstract fun contributesMainActivity():AuthActivity

     @ContributesAndroidInjector(modules = [DashBoardViewModelModules::class,DashBoardModule::class])
     abstract fun contributesDashBoardActivity():DashBoardActivity


}