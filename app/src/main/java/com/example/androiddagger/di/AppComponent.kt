package com.example.androiddagger.di

import android.app.Application
import com.example.androiddagger.factory.ViewModelFactoryModule
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuildersModule::class,
    AppModule::class,
    ViewModelFactoryModule::class
])
interface AppComponent : AndroidInjector<BaseApplication> {


    @Component.Builder
    interface Builder {


        @BindsInstance
        fun application(application: Application): Builder


        fun build(): AppComponent
    }
}