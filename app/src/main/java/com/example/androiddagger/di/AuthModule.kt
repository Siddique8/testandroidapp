package com.example.androiddagger.di

import com.example.androiddagger.networking.AuthApi
import com.example.androiddagger.networking.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.create

@Module
class AuthModule {
    @Provides
    internal fun providesAuthApi(retrofit: Retrofit):AuthApi{
        return retrofit.create(AuthApi::class.java)
    }
}

