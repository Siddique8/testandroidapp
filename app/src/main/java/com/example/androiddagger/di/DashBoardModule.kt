package com.example.androiddagger.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.androiddagger.database.UserDao
import com.example.androiddagger.database.UserDataBase
import com.example.androiddagger.networking.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class DashBoardModule {

    @Provides
    internal fun providesDashBoardModule(retrofit: Retrofit): UserApi{
        return retrofit.create(UserApi::class.java)
    }

    @Provides
    internal fun providesDataBaseInstance(application : Application):UserDataBase {
        return Room.databaseBuilder(application,
             UserDataBase::class.java, UserDataBase.DATA_BASE_NAME)
            .fallbackToDestructiveMigration()
            .build()

    }


}