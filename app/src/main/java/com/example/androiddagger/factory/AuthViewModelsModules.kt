package com.example.androiddagger.factory

import androidx.lifecycle.ViewModel
import com.example.androiddagger.viewmodels.AuthViewModel
import com.example.androiddagger.viewmodels.DashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelsModules {

    @Binds
    @IntoMap
    @ViewModelKeys(AuthViewModel::class)
    abstract fun bindsAuthViewModel(viewModel: AuthViewModel):ViewModel

}