package com.example.androiddagger.factory

import androidx.lifecycle.ViewModel
import com.example.androiddagger.viewmodels.DashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DashBoardViewModelModules {

    @Binds
    @IntoMap
    @ViewModelKeys(DashViewModel::class)
    abstract fun bindDashViewModel(dashViewModel: DashViewModel):ViewModel

}