package com.example.androiddagger.factory

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindsViewModelFactory(modelProviderFactory:ViewModelProviderFactory):ViewModelProvider.Factory


}