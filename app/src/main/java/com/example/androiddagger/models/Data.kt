package com.example.androiddagger.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Data{

     @SerializedName("userId")
     @Expose
     val userId:Int?=null

     @SerializedName("id")
     @Expose
     val id:Int?=null

     @SerializedName("title")
     @Expose
     val title: String?=null

     @SerializedName("completed")
     @Expose
     val isCompleted: Boolean?=null


     var checkId:Int? = null

}