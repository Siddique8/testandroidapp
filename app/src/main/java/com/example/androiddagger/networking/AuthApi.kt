package com.example.androiddagger.networking

import android.provider.ContactsContract
import com.example.androiddagger.models.Data
import com.example.androiddagger.resource.AuthResource
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import javax.annotation.Resource

interface AuthApi {


    @GET("todos/{id}")
    fun authUser(@Path("id") id:Int): Flowable<Data>

}