package com.example.androiddagger.networking

import com.example.androiddagger.models.Data
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.http.GET
import javax.annotation.Resource

interface UserApi {

    @GET("todos")
    fun getUsers(): Flowable<List<Data>>
}