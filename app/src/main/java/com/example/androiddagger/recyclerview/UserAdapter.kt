package com.example.androiddagger.recyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddagger.R
import com.example.androiddagger.database.User
import kotlinx.android.synthetic.main.item_view.view.*

class UserAdapter(private val context: Context): PagedListAdapter<User,UserAdapter.UserViewHolder>(UserDiffCallback()){

   // private var usersList:List<User>?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(LayoutInflater.from(context).inflate(R.layout.item_view,parent,false))
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        var user = getItem(position)

        if (user == null){
            holder.clear()
        }else{
            holder.bind(user)
        }

    }


    class UserViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var tvUserId:TextView = view.findViewById(R.id.tvUserId)
        var tvUserTitle:TextView = view.findViewById(R.id.tvTitle)

        fun bind(user: User){
            tvUserId.setText(user.userId.toString())
            tvUserTitle.setText(user.title)
        }

        fun clear(){
            tvUserId.text=null
            tvUserTitle.text=null
        }


    }

}