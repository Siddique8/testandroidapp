package com.example.androiddagger.resource

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddagger.R
import com.example.androiddagger.database.User
import kotlinx.android.synthetic.main.item_view.view.*

class SimpleRecyclerView() : RecyclerView.Adapter<SimpleRecyclerView.RecyclerViewHolder>() {


    var userList:List<User>?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_view,parent,false))
    }

    override fun getItemCount(): Int {
        return userList!!.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        var user = userList?.get(position)
        holder.bind(user!!)
    }

    fun subList(item:List<User>){
        userList = item
    }


    class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var tvUserId:TextView = view.findViewById<TextView>(R.id.tvUserId)
        var tvTitle:TextView = view.findViewById<TextView>(R.id.tvTitle)

        fun bind(user: User){
            if (user !=null){
                tvUserId.setText(user.userId.toString())
                tvTitle.setText(user.title)
            }
        }

    }

}