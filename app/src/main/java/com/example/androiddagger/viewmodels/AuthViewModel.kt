package com.example.androiddagger.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.androiddagger.models.Data
import com.example.androiddagger.networking.AuthApi
import com.example.androiddagger.resource.AuthResource
import javax.inject.Inject

class AuthViewModel : ViewModel {

     var repository:Repository

    @Inject
    constructor(authApi : AuthApi) {
            repository = Repository(authApi)
          //  repository.observeData()
    }

    fun authenticateUser(id : Int):LiveData<AuthResource<Data>>{
        return repository.authenticateUserWithId(id)
    }




}