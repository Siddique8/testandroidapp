package com.example.androiddagger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MediatorLiveData
import com.example.androiddagger.models.Data
import com.example.androiddagger.networking.UserApi
import com.example.androiddagger.resource.Resource
import com.example.androiddagger.resource.Resource.Companion.error
import com.example.androiddagger.resource.Resource.Companion.loading
import com.example.androiddagger.resource.Resource.Companion.success
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import java.util.*


class DashReposotory {

    var userApi: UserApi

    constructor(userApi: UserApi){
        this.userApi = userApi
    }

    private var data: MediatorLiveData<Resource<List<Data>>>?=null

    fun getUserData(): LiveData<Resource<List<Data>>>? {


        if (data == null) {
            data = MediatorLiveData()
           // data.setValue(loading(List<Data>))
            val source = LiveDataReactiveStreams.fromPublisher(
                    userApi!!.getUsers()
                        .onErrorReturn(object :
                            Function<Throwable?, List<Data>?> {
                            override fun apply(t: Throwable): List<Data> {
                                val d = Data()
                                d.checkId = -1
                                val dataList = ArrayList<Data>()
                                dataList.add(d)
                                return dataList
                            }
                        })
                        .map {
                            if (it.size>0){
                                if (it[0].checkId == -1){
                                     error("Data not found",null)
                                }
                            }
                            success(it)
                        }
//                        .map<Resource<List<Data>>>(Function<List<Data>, Resource<List<Data>?>?> { data ->
//                                if (data.size > 0) {
//                                    if (data[0].checkId == -1) {
//                                        return  error("Data Not Found", null)
//                                    }
//                                }
//                                success(data)
//                            })
                        .subscribeOn(Schedulers.io())
                )

            data!!.addSource(source) { listResource ->
                data!!.setValue(listResource)
                data!!.removeSource(source)
            }
        }
        return data
    }


}