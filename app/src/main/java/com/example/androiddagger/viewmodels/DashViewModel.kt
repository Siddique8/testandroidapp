package com.example.androiddagger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.androiddagger.database.NetworkBoundResource
import com.example.androiddagger.database.User
import com.example.androiddagger.database.UserDataBase
import com.example.androiddagger.models.Data
import com.example.androiddagger.networking.UserApi
import com.example.androiddagger.resource.Resource
import javax.inject.Inject
class DashViewModel : ViewModel {

    var dashRepository: DashReposotory
    private var userPagedList:LiveData<PagedList<User>>

    @Inject
    constructor(userApi:UserApi,dataBase: UserDataBase){
        dashRepository = DashReposotory(userApi)

        val factory: DataSource.Factory<Int,User> = dataBase.userDao().getPagedUsers()
        val pagedListBuilder: LivePagedListBuilder<Int, User> = LivePagedListBuilder<Int,User>(factory,10)
        userPagedList = pagedListBuilder.build()

    }
    fun getUsersPagedData() = userPagedList

    fun observeUserData():LiveData<Resource<List<Data>>>?{
        return dashRepository.getUserData()
    }

}