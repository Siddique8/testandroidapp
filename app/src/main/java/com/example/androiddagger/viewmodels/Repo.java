package com.example.androiddagger.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.example.androiddagger.models.Data;
import com.example.androiddagger.networking.UserApi;
import com.example.androiddagger.resource.Resource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class Repo {

    private UserApi userApi;
    private MediatorLiveData<Resource<List<Data>>> data;

    public LiveData<Resource<List<Data>>> getUserData(){

        if (data ==null){


            data = new MediatorLiveData<>();
            data.setValue(Resource.Companion.loading((List<Data>)null));

            final LiveData<Resource<List<Data>>> source = LiveDataReactiveStreams.fromPublisher(

                    userApi.getUsers()
                    .onErrorReturn(new Function<Throwable, List<Data>>() {
                        @Override
                        public List<Data> apply(Throwable throwable) throws Exception {

                            Data d = new Data();
                            d.setCheckId(-1);
                            ArrayList<Data> dataList = new ArrayList<>();
                            dataList.add(d);
                            return dataList;
                        }
                    }).map(new Function<List<Data>, Resource<List<Data>>>() {
                        @Override
                        public Resource<List<Data>> apply(List<Data> data) throws Exception {

                            if (data.size()>0){
                                if (data.get(0).getCheckId()==-1){
                                    return Resource.Companion.error("Data Not Found",null);
                                }
                            }
                            return Resource.Companion.success(data);
                        }
                    })
                    .subscribeOn(Schedulers.io())
            );

            data.addSource(source, new Observer<Resource<List<Data>>>() {
                @Override
                public void onChanged(Resource<List<Data>> listResource) {
                    data.setValue(listResource);
                    data.removeSource(source);
                }
            });



        }

        return data;

    }

}
