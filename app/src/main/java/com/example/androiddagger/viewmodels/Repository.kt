package com.example.androiddagger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.example.androiddagger.models.Data
import com.example.androiddagger.networking.AuthApi
import com.example.androiddagger.resource.AuthResource
import com.example.androiddagger.resource.AuthResource.Companion.authenticated
import com.example.androiddagger.resource.AuthResource.Companion.error
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers


class Repository {

    private var authApi: AuthApi? = null

    constructor(authApi: AuthApi) {
        this.authApi = authApi
    }

    fun authenticateUserWithId(id: Int): LiveData<AuthResource<Data>> {
        return LiveDataReactiveStreams.fromPublisher(
            authApi!!.authUser(id)
                .onErrorReturn(object:
                    Function<Throwable?, Data?> {
                    override fun apply(t: Throwable): Data {
                        val errorUser = Data()
                        errorUser.checkId = -1
                        return errorUser
                    }
                }).map<AuthResource<Data>>({
                    if (it.checkId == -1){
                        error("could not authenticate User ", Data())
                    }
                    else {
                        authenticated(it)
                    }
                })
                .subscribeOn(Schedulers.io())
        )
    }
}
